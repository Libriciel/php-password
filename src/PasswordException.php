<?php

namespace Libriciel\Password;

use RuntimeException;

class PasswordException extends RuntimeException
{
    //Nothing to do
}
